import os


def require_env_variable(name):
    val = os.environ.get(name)
    if val is None:
        raise Exception(
            "Must have defined environment variable %s" % name)

    return val
