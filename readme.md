**This add-on has been replaced by native Bitbucket support for HipChat.  Feel free to use it as an example or fork it for your own purposes, but it is no longer available on the Marketplace.**

An example HipChat Add-on that integrates with Bitbucket to display commits in a room.

Differences to old Bitbucket connector:
- Emoticon support